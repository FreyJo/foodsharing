### General locations

#### Components
`/client/src/components`
#### Views
Here are all complete elements of the website located, but separated between a full page like the `Dashboard` or a partials like `Topbar` or `Footer`.

`/client/src/view`
##### Page views
Here is the main content stored, like `Index` or `Dashboard`.

`/client/src/view/pages`

##### Partials views
Here are all the parts of the website, like the `Topbar` or `Footer`.

`/client/src/view/partials`
